const Passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const { validationResult } = require("express-validator");
const RememberMeStrategy = require("passport-remember-me").Strategy;
const cookieParser = require("cookie-parser");

const UserModel = require("../models/user");
const {
  removeToken,
  updateToken,
  setToken,
  handleIncomingToken,
  findUserByEmailOrName,
  registerNewUser,
} = require("./utils");

module.exports = {
  configure: (app) => {
    app.use(
      require("express-session")({
        secret: "Massive Secret Test",
        resave: false,
        saveUninitialized: false,
      })
    );
    app.use(cookieParser());
    app.use(Passport.initialize());
    app.use(Passport.session());
    Passport.use(new LocalStrategy(UserModel.authenticate()));
    Passport.use(new RememberMeStrategy(handleIncomingToken, updateToken));
    Passport.serializeUser(UserModel.serializeUser());
    Passport.deserializeUser(UserModel.deserializeUser());
    app.use(Passport.authenticate("remember-me"));
  },
  homepage: (req, res) => res.send('Homepage <a href="/logout">Logout</a>'),
  getLoginView: (req, res) =>
    res.render("login", {
      errorMessage: "",
    }),
  login: async (req, res, next) => {
    const { rememberMe } = req.body;
    let { email } = req.body;
    const errors = validationResult(req).mapped();
    if (errors.password) {
      return res.render("login", {
        errorMessage: "Password not provided",
      });
    }
    if (email) {
      email = email.toLowerCase();
    }
    if (errors.username && errors.email) {
      return res.render("login", {
        errorMessage: "Username or email not provided",
      });
    }
    if (errors.username && !errors.email) {
      req.body.username = email;
    }
    Passport.authenticate("local", (err, user) => {
      if (err) {
        return res.render("login", { errorMessage: "Error: " + err });
      }
      if (!user) {
        return res.render("login", {
          errorMessage: "Incorrect Login",
        });
      }
      req.logIn(user, async (err) => {
        if (err) {
          return next();
        }
        if (rememberMe) {
          try {
            const { token } = await setToken(user._id);
            res.cookie("remember_me", token, {
              path: "/",
              httpOnly: true,
              maxAge: 604800000,
            });
          } catch (error) {
            return next(error);
          }
        }
        return res.redirect("/");
      });
    })(req, res);
  },
  getSignupView: (req, res) =>
    res.render("signup", {
      errorMessage: "",
    }),
  signup: async (req, res, next) => {
    const { username, password, email, rememberMe } = req.body;
    const errors = validationResult(req).mapped();
    if (errors.username) {
      return res.render("signup", { errorMessage: "Username not provided" });
    }
    if (errors.password) {
      return res.render("signup", { errorMessage: "Password not provided" });
    }
    if (errors.email) {
      return res.render("signup", {
        errorMessage: "Email not provided or invalid",
      });
    }
    const user = await findUserByEmailOrName(email, username).catch((error) =>
      console.log("Error finding userModel in signup: %O", error)
    );
    if (user) {
      return res.render("signup", {
        errorMessage:
          "Username or email has been already taken by another user.",
      });
    }
    const newUser = await registerNewUser(username, email, password).catch(
      (error) => {
        return res.render("signup", { errorMessage: error });
      }
    );
    if (newUser) {
      Passport.authenticate("local")(req, res, async () => {
        if (rememberMe) {
          const { token } = await setToken(newUser._id);
          res.cookie("remember_me", token, {
            path: "/",
            httpOnly: true,
            maxAge: 604800000,
          });
        } else {
          req.logIn(newUser, (error) => {
            if (error) {
              return next();
            }
          });
        }
        return res.redirect("/");
      });
    }
  },
  isLoggedIn: (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    }
    return res.redirect("/login");
  },
  logout: async (req, res) => {
    const token = req.cookies.remember_me;
    if (token) {
      await removeToken(token);
    }
    res.clearCookie("remember_me");
    req.logout();
    return res.redirect("/login");
  },
};
