1. To launch the project - run the following commands in the terminal "yarn" and "yarn start" (if you use npm - use "npm install" and "npm run start").
2. Form validation works properly only on the signup form (as it was specified in the task);
3. The code was partially rewritten in “promise style”, with async-await functions code looks more linear and better to read. ( It is a “standard” of node js development now );
4. nodemon for the auto-reload server was added to the project;
5. Also project used eslint in order to keep code more consistent;
6. ejs part was rewritten on layout and pages (to comply with DRY principles).
7. For “remind me functionality” was used package “passport-remember-me” that interacts with the “passport” library for auth and registration. In order to implement this functionality, was created one more collection in MongoDB, for storing tokens.

That's why even after the server is reloaded, “remember me” is still working. When the user signs in with the ‘remember me’ flag, after each request, the token is being changed for security reasons.