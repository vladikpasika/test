const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const http = require("http");
const fileUpload = require("express-fileupload");
const expressLayouts = require('express-ejs-layouts');

const routes = require("./routes/routes");
const controller = require("./controllers/controller");

const app = express();
const server = http.createServer(app);

const databaseName = "Test";

mongoose.connect("mongodb://localhost/" + databaseName, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  console.log("Connected to database: " + databaseName);
});

// Configure Passport Users
controller.configure(app);

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(fileUpload());
app.set("secretKey", "DaveIsCool");

app.set("views", "public/views/pages");
app.set("view engine", "ejs");
 
app.use(expressLayouts);

app.use(express.static("public"));
app.use(express.static("files"));
app.use("/", routes);

server.listen(process.env.PORT || 80, function () {
  console.log(`Starting Server on port: ${process.env.PORT}`);
});
