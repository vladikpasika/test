const { v4: uuidv4 } = require("uuid");

const UserModel = require("../models/user");
const TokenModel = require("../models/token");

exports.generateUid = () => uuidv4();

exports.saveRememberMeToken = async (token, uId) =>
  await TokenModel.findOneAndUpdate(
    { uId },
    { token, uId },
    { new: true, upsert: true, useFindAndModify: false }
  );

exports.setToken = async (uId) => {
  const token = exports.generateUid();
  return await exports.saveRememberMeToken(token, uId);
};

exports.updateToken = async (tokenEntity, done) => {
  const token = exports.generateUid();
  try {
    const result = await exports.saveRememberMeToken(token, tokenEntity.uId);
    done(null, result.token);
  } catch (error) {
    done(error);
  }
};

exports.consumeRememberMeToken = async (token) => {
  const result = await TokenModel.findOne({
    token,
  });
  if (result && result.uId) {
    return await UserModel.findOne({ _id: result.uId });
  }
  return null;
};

exports.removeToken = async (token) => await TokenModel.deleteOne({ token });

exports.handleIncomingToken = async (token, done) => {
  try {
    const user = await exports.consumeRememberMeToken(token);
    if (user) {
      await exports.removeToken(token);
      return done(null, user);
    }
    return done(null, false);
  } catch (error) {
    return done(error);
  }
};

exports.findUserByEmailOrName = async (email, username) =>
  await UserModel.findOne({
    $or: [{ email }, { username }],
  });

exports.registerNewUser = async (username, email, password) =>
  await UserModel.register(
    new UserModel({
      username: username,
      email: email.toLowerCase(),
    }),
    password
  );
