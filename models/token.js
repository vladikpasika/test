const mongoose = require("mongoose");

const TokenSchema = new mongoose.Schema({
  token: String,
  uId: String,
});


module.exports = mongoose.model("RMToken", TokenSchema);
