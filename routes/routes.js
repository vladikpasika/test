const express = require("express");
const { check } = require("express-validator");

const controller = require("../controllers/controller");

const router = express.Router();
const isLoggedIn = controller.isLoggedIn;

router.get("/", isLoggedIn, controller.homepage);
router.get("/login", controller.getLoginView);
router.post(
  "/login",
  [
    check("email").isEmail().not().isEmpty(),
    check("username").isString().not().isEmpty(),
    check("password").isString().not().isEmpty(),
  ],
  controller.login
);
router.get("/signup", controller.getSignupView);
router.post(
  "/signup",
  [
    check("email").isEmail().not().isEmpty(),
    check("username").isString().not().isEmpty(),
    check("password").isString().not().isEmpty(),
  ],
  controller.signup
);
router.get("/logout", controller.logout);

module.exports = router;
